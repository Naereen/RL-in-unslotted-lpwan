%   EVOL_COLPROB_TD In this file we analyze the evolution of the probability of collision
%   versus Td the interval between the uplink packet and the acknowledgement
%   obtained.

%   Other m-files required: none

%   Subfunctions: none
%   MAT-files required: none
%
%   See also : none

%   This file is the code used for the simulations done in the paper:
%   Bonnefoi, R.; Moy, C.; Palicot, J. “Improvement of the LPWAN AMI Backhaul's Latency 
%   thanks to Reinforcement Learning Algorithms”,  Eurasip JWCN, 2018. 

%   Author  : Rémi BONNEFOI
%   SCEE Research Team - CentraleSupélec - Campus de Rennes
%   Avenue de la Boulaie 35576 Rennes CEDEX CS 47601 FRANCE
%
%   Email : remi.bonnefoi@centralesupelec.fr
%   Website : remibonnefoi.wordpress.com
%
%   Last revision : 01/30/2018


% System parameters:
Ta              = [20 30 100 180]; % Ta is the duration of the acknowledgement
Tm              = [40 70 160 280]; % Tm is the length of the uplink packet
Td              = 0:10:400; % Td is the interval between the uplink packet and the acknowledgement

lambda          = 0.002;        % lambda is equal to 0.2s^-1

% Result
P_success       = zeros(length(Tm),length(Td));

for j=1:1:length(Tm)
    
        % Formulas

        % Td>Tm

        Num             = exp(-2*lambda*Tm(1,j)).*exp(-lambda*(Tm(1,j)+Ta(1,j)));
        Den             = 1 + exp(-lambda*(Td+Tm(1,j)))-exp(-lambda*(Td+Tm(1,j)+Ta(1,j)))+(exp(-lambda*(Tm(1,j)))-exp(-lambda*(Tm(1,j)+Ta(1,j)))).*(1/(lambda*Ta(1,j))).*(exp(-lambda*(Tm(1,j)))-exp(-lambda*(Tm(1,j)+Ta(1,j)))-exp(-lambda*(Td))+exp(-lambda*(Td+Ta(1,j))));

        Pth1            = Num./Den;

        % Td<Tm

        Num             = exp(-lambda*(2*Tm(1,j)+Td+Ta(1,j)));
        Den             = 1+exp(-lambda*(Tm(1,j)+Td))-exp(-lambda*(Tm(1,j)+Td+Ta(1,j)));

        Pth2            = Num./Den;


% Choice between the two probabilities
IndexTm         = find(Td==Tm(1,j));
P_success(j,:)  = [Pth2(:,1:IndexTm-1) Pth1(:,IndexTm:length(Td))];

end

figure;
hold on;
grid on;
box on;
plot(Td/100,P_success(1,:),'-+');
plot(Td/100,P_success(2,:),'-v');
plot(Td/100,P_success(3,:),'-o');
plot(Td/100,P_success(4,:),'-s');
ylabel('Probability of successful transmission');
xlabel('T_d (s)');
legend('T_m = 0.4s, T_a = 0.2s','T_m = 0.7s, T_a = 0.3s','T_m = 1.6s, T_a = 1s','T_m = 2.8s, T_a = 1.8s');
set(findall(gcf,'type','axes'),'fontsize',13)