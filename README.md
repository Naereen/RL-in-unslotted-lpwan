### Reinforcement Learning in unslotted LPWAN

This repository contains the code which has been used in order to generate the simulation results done for our article: 

Bonnefoi, R.; Moy, C.; Palicot, J., “Improvement of the LPWAN AMI Backhaul's Latency thanks to Reinforcement Learning Algorithms”, *Eurasip Journal on Wireless Communications and Networking* , 2018.

This article can be found on the Eurasip's website. Another version is available on HAL: https://hal.archives-ouvertes.fr/hal-01697736/document

### Short description 

This repository contains six files:

* The first one, "Prob_col_network.m" is used to verify our analytical formula for the probability of collision in a LoRaWAN network.
* The second one, "Evol_colprob_Td.m" is used to analyze the evolution of the probability of collision versus the latency in the network
* the four others allow to assess the performance of learning algorithms in the studied LPWAN.

### License

[MIT Licensed](https://mit-license.org/) (file [LICENSE.txt](LICENSE.txt)).